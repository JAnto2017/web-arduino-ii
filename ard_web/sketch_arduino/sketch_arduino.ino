#include "VSync.h"


/**
 * Once the Arduino sketch receives the output data from Processing
 * It will trigger the LED signal.
 * If the output is 1 it will turn on the LED
 * If the output is 2 it will turn off the LED
 * 
 * Before uploading the sketch, make sure that Processing isn't running
 * because it will using the same port and a 'port busy' error will come up
 * 
 * Including the library that will help us in receiving and sending the values from processing
 */

ValueReceiver<1> receiver;
ValueSender<1> sender;
int output;
int input;
int ledPin = 13;
//-----------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  pinMode(ledPin,OUTPUT);
  receiver.observe(output);
  sender.observe(input);
}
//-----------------------------------------------------------------
void loop() {
  receiver.sync();
  if (output == 1) {
    digitalWrite(ledPin,HIGH);
    input = 1;
  } else if (output == 2) {
    digitalWrite(ledPin,LOW);
    input = 2;
  }
  sender.sync();
}
