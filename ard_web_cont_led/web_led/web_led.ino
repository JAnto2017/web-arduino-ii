/**
 * https://www.thegeekpub.com/16569/controlling-an-arduino-from-a-web-page/
 * 
 * Controlling ar Arduino from a web page
 * 
 */
 
#include <Ethernet.h>
#include <SPI.h>
//-----------------------------------------------------------------------------------------
byte mac[] = {0xDE,0xAD,0xBE,0xAD,0xEE,0xBE};
byte ip[] = {192,168,1,212};
byte gateway[] = {192,168,1,1};
byte subnet[] = {255,255,255,0};
//-----------------------------------------------------------------------------------------
EthernetServer server(80);
String controlString;
int blueLEDpin = 2;
//-----------------------------------------------------------------------------------------
void setup() {
  pinMode(blueLEDpin, OUTPUT);
  Ethernet.begin(mac,ip,gateway,subnet);
  server.begin();
}
//-----------------------------------------------------------------------------------------
void loop() {
  EthernetClient client = server.available();
  if (client) {
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();

        // read the HTTP request
        if (controlString.length() < 100) {
          controlString += c;       //write character to string
        }

        //if HTTP request has ended 0x0D is carriage return \n ASCII
        if (c == 0x0D) {
          client.println("HTTP/1.1 200 OK"); //send new page
          client.println("Content-Type: text/html");
          client.println(); 
          client.println("<html>");
          client.println("<head>");
          client.println("<title>WEB-LED</title>");
          client.println("</head>");
          client.println("<body>");
          //client.println("<img src=\"https://cdn.thegeekpub.com/wp-content/uploads/2018/01/the-geek-pub-big-logo-new.jpg\") style=\"width: 55%; margin-left: auto; margin-right: auto; display: block;\" />");
          client.println("<h1 style=\"color: blue; font-family: arial; text-align: center;\">CONTROL ARDUINO ETHERNET VIA WEB</h1>");
          client.println("<h2 style=\"color: green; font-family: arial; text-align: center;\">LED ON/OFF DESDE PÁGINA WEB</h2>");
          client.println("<hr>");
          client.println("<h2 style=\"color: blue; font-family: arial; text-align: center;\"><a href=\"/?GPLED2ON\"\">ON LED</a> === <a href=\"/?GPLED2OFF\"\">OFF LED</a></h2>");
          client.println("</body>");
          client.println("</html>");

          delay(10);
          client.stop();

          //control arduino pin
          if(controlString.indexOf("?GPLED2ON") > -1) { //check for LED ON
            digitalWrite(blueLEDpin, HIGH);
          } else {
            if (controlString.indexOf("?GPLED2OFF") > -1) {
              digitalWrite(blueLEDpin, LOW);
            }
          }
          controlString = ""; //clearing string for next read
        }
      }
    }
  }
}
