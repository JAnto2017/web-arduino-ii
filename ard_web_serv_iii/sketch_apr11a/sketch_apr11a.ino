/**
 * https://roboticsandenergy.com/projects/arduino-projects/arduino-web-server-led-control/
 * 
 * Arduino Web Server LED Control
 * Páginas web alojadas en tarjeta SD
 * 
 */

#include <SD.h>
#include <Ethernet.h>
#include <SPI.h>
//-----------------------------------------------------------------------------------
#define REQ_BUF_SZ 40 //tamaño del buffer usado para capturar las peticiones HTTP
//-----------------------------------------------------------------------------------
#define IP IPAddress(192,168,1,212)
//-----------------------------------------------------------------------------------
byte mac[] = {0xDE,0xAD,0xBE,0xEF,0xFE,0xED};
EthernetServer server(80);
File webFile;
char HTTP_req[REQ_BUF_SZ] = {0};
byte req_index = 0;
bool root = true;
//-----------------------------------------------------------------------------------
#define RED 3
#define GREEN 5
#define BLUE 6
#define PWR 2
//-----------------------------------------------------------------------------------
#define SENSOR A0
//-----------------------------------------------------------------------------------
/**
 * funcción que establece todos los elementos string a 0 (limpia el array)
 */
void StrClear(char *str, char len) {
  for (int i=0; i<len; i++)
    str[i] = 0;
}
//-----------------------------------------------------------------------------------
/**
 * busca los string sfind en los string str
 * retorna 1 si el string es encontrado
 * retorna 0 si el string no es encontrado
 */
byte StrContains(char *str, char *sfind) {
  #define LEN strlen(str)
  byte found = 0;
  byte index = 0;

  if (strlen(sfind) > LEN) return 0;
  while (index < LEN) {
    if (str[index] == sfind[found]) {
      found++;
      if (strlen(sfind) == found) return index;
    } else found = 0;
    index++;
  }
  return 0;
}
//-----------------------------------------------------------------------------------
/**
 * byte = 00001111 (0000rgbv)
 */
void controlHandler(byte output) {
  digitalWrite(PWR, output%2);
  digitalWrite(BLUE, (output/2)%2);
  digitalWrite(GREEN, (output/4)%2);
  digitalWrite(RED, (output/8)%2);
}
//-----------------------------------------------------------------------------------
/**
 * **********************************************************************************
 */
void setup() {
  
  pinMode(RED,OUTPUT);
  pinMode(GREEN,OUTPUT);
  pinMode(BLUE,OUTPUT);
  pinMode(PWR,OUTPUT);
  
  digitalWrite(PWR,LOW);
  digitalWrite(RED,HIGH);
  digitalWrite(GREEN, HIGH);
  digitalWrite(BLUE, HIGH);

  Serial.begin(9600);

  Ethernet.begin(mac, IP);
  server.begin();
  
  //inicializa SD card
  Serial.println("Inicializa tarjeta SD...");

  if (!SD.begin(4)) {
    //4 - SS pin
    Serial.println("Error - SD card no inicia");
    return;
  }
  Serial.println("Success - SD card inicializada");
  
}
//-----------------------------------------------------------------------------------
/**
 * **********************************************************************************
 */
void loop() {
  EthernetClient client = server.available();

  if (client) {
    bool currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        if (req_index < (REQ_BUF_SZ - 1)) {
          HTTP_req[req_index] = c;
          req_index++;
        }
        Serial.print(c);
        if (c == '\n' && currentLineIsBlank) {
          if (root) {
            if (StrContains(HTTP_req, "GET /") || StrContains(HTTP_req, "GET /login.html")) {
              root = false;
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/html");
              client.println("Connnection: close");
              client.println();
              webFile = SD.open("login.html");
            } else if(StrContains(HTTP_req, "control.html")) {
              root = true;
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/html");
              client.println("Connnection: close");
              client.println();
              webFile = SD.open("control.html");
            } else if (StrContains(HTTP_req, "main.html")) {
              root = true;
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/html");
              client.println("Connnection: close");
              client.println();
              webFile = SD.open("main.html");
            } else if (StrContains(HTTP_req, "servWebArd_SD_2.png")) {
              client.println("HTTP/1.1 200 OK");
              client.println();
              webFile = SD.open("servWebArd_SD_2.png");
            } else if (StrContains(HTTP_req, "style.css")) {
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/css");
              client.println("Connnection: close");
              client.println();
              webFile = SD.open("style.css");
            } else if (StrContains(HTTP_req, "click?")) {
              client.println("HTTP/1.1 200 OK");
              client.println();
              byte i = StrContains(HTTP_req, "click?") + 2;
              byte result = 0;
              byte dig = 0;
              for(i; HTTP_req[i] != 'x'; i++) {
                dig++;
                if(dig >= 2) result *= 10;       //if dig >= 2 then move the digits left e.g. 24 => 240 
                  result += (HTTP_req[i] - '0');                        
              }
                controlHandler(result);
            } else {
              client.println("HTTP/4.5 404 Not Found");
              client.println("Content-Type: text/html");
              client.println("Connnection: close");
              client.println();
              webFile = SD.open("404.html");
            }            
          } else {
            // go to login page if client is noth authorized
            if (StrContains(HTTP_req, "GET / ") || StrContains(HTTP_req, "GET /login.html")) {
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/html");
              client.println("Connection: close");
              client.println();
              webFile = SD.open("login.html");
            } else if (StrContains(HTTP_req, "user=root&pass=1234")) {
              //this is a login event. Login if the user and password details entered are "ro
              root = true;
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/html");
              client.println("Connection: close");
              client.println();
              webFile = SD.open("main.html");
            } else if (StrContains(HTTP_req, "style.css")) {
              client.println("HTTP/1.1 200 OK");
              client.println("Content-Type: text/css");
              client.println("Connection: close");
              client.println();
              webFile = SD.open("style.css");
            } else {
              client.println("HTTP/4.2 401 Unauthorized");
              client.println("Content-Type: text/html");
              client.println("Connection: close");
              client.println();
              webFile = SD.open("401.html");
            }
          }
          // If a request to open a page was received then webfile should be loaded.
          if (webFile) {
            while(webFile.available()) {
              client.write(webFile.read()); //send web page to client
            }
            webFile.close();
          }
          //reset buffer inedx and all buffer elements to 0
          req_index = 0;
          StrClear(HTTP_req, REQ_BUF_SZ);
          break;
        }
        //every line of text received from the client ends with \r\n
        if (c == '\n') {
          //last character on line of received text
          //starting new line with next character read
          currentLineIsBlank = true;
        } else if(c != '\r') {
          //a text character was received from client
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);       //give the web browser time to receive the data
    client.stop();  //close the connection
  }

}
