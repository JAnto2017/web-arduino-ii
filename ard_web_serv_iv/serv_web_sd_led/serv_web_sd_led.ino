/**
 * Almacenar página web en tarjeta SD - index.html
 * Desde la página web se puede encender/apagar LED
 * 
 */
#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
//---------------------------------------------------------------------------------
byte mac[] = {0xDE,0xAD,0xBE,0xAD,0xEE,0xBE}; 
byte ip[] = {192,168,1,213}; 
byte gateway[] = {192,168,1,1}; 
byte subnet[] = {255,255,255,0}; 

EthernetServer server(80);
File webFile;
String controlString;
int blueLEDpin = 2;
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
    
  Serial.println("Ethernet WebServer");
  Ethernet.begin(mac,ip,gateway,subnet);
  server.begin();
  //Serial.print("Server is at ");
  
  if (!SD.begin(4)) {
    //4 - SS pin
    Serial.println("Error - SD card no inicia");
    return;
  } else {
    Serial.println("Success - SD card inicializada");
  }
  pinMode(blueLEDpin,OUTPUT);
}
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
void loop() {
  EthernetClient client = server.available();
  
  if (client) {
    Serial.println("Nuevo cliente");
    //bool currentLineIsBlank = true;
    
    while (client.connected()) {
      if (client.available()){
        
        char c = client.read();
        //Serial.write(c);

        // read the HTTP request
        if (controlString.length() < 100) {
          controlString += c;       //write character to string
        }

        if (c == 0x0D) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          //client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          webFile = SD.open("index.html");
          break; 
          /*
          client.println("<html>");
          client.println("<head>");
          client.println("<title>WEB-LED</title>");
          client.println("</head>");
          client.println("<body>");    
          client.println("<h1 style=\"color: blue; font-family: arial; text-align: center;\">CONTROL ARDUINO ETHERNET VIA WEB</h1>");
          client.println("<h2 style=\"color: green; font-family: arial; text-align: center;\">LED ON/OFF DESDE PÁGINA WEB</h2>");
          client.println("<hr>");
          client.println("<h2 style=\"color: blue; font-family: arial; text-align: center;\"><a href=\"/?GPLED2ON\"\">ON LED</a> === <a href=\"/?GPLED2OFF\"\">OFF LED</a></h2>");
          client.println("</body>");
          client.println("</html>");   
          */  
        }

        
        
      }
    }

    delay(1);
    client.stop();
    Serial.println("Cliente desconectado");
    
  }//if
}
