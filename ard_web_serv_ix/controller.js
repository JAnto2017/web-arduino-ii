//https://github.com/Hallessandro/led-controller/blob/master/index.js
var five = require('johnny-five');
var http = require('http');
const express = require('express');
const app = express();

var board = new five.Board();
var isReady = false;
var isOn = false;
var led;

app.listen(3000, () => {
    console.log("Servidor activo en puerta 3000");
});

board.on('ready', function() {
    led = new five.Led(13);
    led.off();
    isReady = true;
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","*");
    next();
});

app.route("/")
    .get((req, res) => {
        toggleLed();
        res.json({status: isOn});
    });



function toggleLed() {
    
    if (!isReady) {return;}

    if (isOn) {
        led.off();
        isOn = false;
    } else {
        led.on();
        isOn = true;
    }
}