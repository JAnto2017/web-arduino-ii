/*
 * https://startingelectronics.org/tutorials/arduino/ethernet-shield-web-server-tutorial/SD-card-web-server/
 * 
 * index.html en Tarjeta SD - Se debe inicializar antes que iniciar el Ethernet!.
 * 
 * Servidor Web
 */
//------------------------------------------------------------------------
#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
//------------------------------------------------------------------------
byte mac[] = {0xDE,0xAD,0xBE,0xEF,0xFE,0xED};
IPAddress ip(192,168,1,131);
EthernetServer server(80);
File webFile;
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void setup() {
  
  Serial.begin(9600);

  Serial.println("Initializing SD card...");
  if (!SD.begin(4)) {
    Serial.println("ERROR - SD card initialization faild!");
    return;
  }
  Serial.println("SUCCESS - SD card initialized");

  Ethernet.begin(mac,ip);
  server.begin();
  
  if (!SD.exists("index.htm")) {
    Serial.println("ERROR - Can't find index.htm file!");
    return;
  }
  Serial.println("SUCCESS - Found index.htm file");
}
//------------------------------------------------------------------------
//------------------------------------------------------------------------
void loop() {
  EthernetClient client = server.available();

  if (client) {
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        if (c == '\n' && currentLineIsBlank) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();
          //send web paga
          webFile = SD.open("index.htm");
          if (webFile) {
            while(webFile.available()) {
              client.write(webFile.read());
            }
            webFile.close();
          }
          break;
        }
        //every line of text received from the client
        if (c == '\n') {
          //last character on line of received text
          //starting new line with next character
          currentLineIsBlank = true;
        } else if (c != '\r') {
          //a text character was received from client
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);
    client.stop();
  }
}
