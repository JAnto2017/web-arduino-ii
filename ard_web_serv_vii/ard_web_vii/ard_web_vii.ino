/*
 * https://startingelectronics.org/tutorials/arduino/ethernet-shield-web-server-tutorial/web-server-LED-control/
 * 
 * index.htm en SD card
 * 
 * Servidor Web 
 * Acción sobre LED 
 */
#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
//-----------------------------------------------------------------------------
byte mac[] = {0xDE,0xAD,0xBE,0xEF,0xFE,0xED};
IPAddress ip(192,168,1,157);
EthernetServer server(80);
File webFile;
//-----------------------------------------------------------------------------
String HTTP_req;
boolean LED_status = 0;
//-----------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);

  Serial.println("Initializing SD card...");
  if (!SD.begin(4)) {
    Serial.println("ERROR - SD card initialization faild!");
    return;
  }
  Serial.println("SUCCESS - SD card initialized");

  
  
  if (!SD.exists("index.htm")) {
    Serial.println("ERROR - Can't find index.htm file!");
    return;
  }
  Serial.println("SUCCESS - Found index.htm file");
  
  Ethernet.begin(mac,ip);
  server.begin();
  Serial.println("SUCCESS - Initialized Ethernet after index.htm was found");

  pinMode(2, OUTPUT);
}
//-----------------------------------------------------------------------------
void loop() {
  // put your main code here, to run repeatedly:

}
