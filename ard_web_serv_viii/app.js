const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const five = require('johnny-five');
const board = new five.Board();
const EVENTS = {
    MOTION_DETECTED: 'motion detected'
};
//---------------------------------------------------------------------------
board.on('ready', function() {
    const led = new five.Led(13);
    const motion = new five.IR.Motion(12);

    io.on('connection', (socket) => {
        motion.on('motionstart', () => {
            console.log('motionstart', Date.now());
            led.on();
            socket.emit(EVENTS.MOTION_DETECTED);
        });
        motion.on('motionend', () => {
            console.log('motionend', Date.now());
            led.off();
        });
    });
});
//---------------------------------------------------------------------------
app.use('/public',express.static({__dirname}/public));
app.get('/', (req,res) => {
    res.redirect('/public/index.html');
});
//---------------------------------------------------------------------------
http.listen(3000, () => {
    console.log('Listenning on *:3000');
});
//---------------------------------------------------------------------------
//when you connect to the root of the server, in this case, localhost:3000/, it will redirect you to the front-end asset