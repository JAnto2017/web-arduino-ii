Internet of Thing IoT
Using Node, Johnny-Five, Arduino

https://www.codeguru.com

npm install -- save johnny-five
npm install -- save-dev nodemon
npm install -- save express socket.io johnny-five

-----------------------------------------------------
--------- EJEMPLO DE package.json -------------------
-----------------------------------------------------
{
   "name": "iot",
   "version": "1.0.0",
   "description": "IoT using JavaScript",
   "main": "app.js",
   "scripts": {
      "start": "node_modules/.bin/nodemon"
   },
   "author": "Andrew Allbright",
   "license": "MIT",
   "dependencies": {
      "express": "^4.14.0",
      "johnny-five": "^0.9.60",
      "socket.io": "^1.4.8"
   },
   "devDependencies": {
      "nodemon": "^1.9.2"
   }
}
//-----------------------------------------------------------
Both 'public/index.html' and 'public/js/index.js' make up the
client code.

A socket connect to the express server is established and, 
whenever the server emits that the Arduino's motion sensor
has been triggered, it displays an alert-styled card.