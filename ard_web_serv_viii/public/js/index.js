const { reset } = require("nodemon");

(function () {
    var TIMER_UPDATE = 10;
    var TIMEOUT = 2750;
    var state = {
        accumulator: 0,
        interval: undefined
    };
    $(function () {
        var socket = io();
        var $display = $('#display');
        var $timer = $('#timer');
        var reset = function () {
            state.interval = clearInterval(state.interval);
            state.accumulator = 0;
            $timer.text('0');
            $display.hide();
        };
        var intervalUpdate = function () {
            state.accumulator += TIMER_UPDATE;
            if (state.accumulator > TIMEOUT) {
                reset();
            } else {
                $timer.text(TIMEOUT - state.accumulator);
            }
        };
        reset();
        socket.on('motion detected', function () {
            $display.show();
            state.interval = setInterval(intervalUpdate, TIMER_UPDATE);
        });
    });
});